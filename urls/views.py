from django.shortcuts import render, redirect
from django.conf import settings

from urls.forms import CutUrlForm
from urls.utils import get_shorten_url
from urls.models import TinyUrl


def index_view(request):
    short_url = None

    if request.method == 'POST':
        form = CutUrlForm(request.POST)
        if form.is_valid():
            short_url = get_shorten_url(form.cleaned_data['url_field'])
    else:
        form = CutUrlForm()

    return render(request, 'index.html', {'form': form, 'short_url': short_url})


def redirect_view(request, shortcut):
    short_url = settings.SITE_URL + shortcut
    real_url = TinyUrl.objects.get(short_url=short_url).real_url
    return redirect(real_url)
