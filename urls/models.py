from django.db import models


class TinyUrl(models.Model):
    short_url = models.CharField(max_length=32, verbose_name='Tiny url')
    real_url = models.URLField(verbose_name='Real url')

    def __str__(self):
        return self.real_url + ' --> ' + self.short_url
