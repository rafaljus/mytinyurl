# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TinyUrl',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('Tiny url', models.CharField(verbose_name='Tiny url', max_length=16)),
                ('real_url', models.URLField()),
            ],
        ),
    ]
