# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('urls', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tinyurl',
            name='Tiny url',
        ),
        migrations.AddField(
            model_name='tinyurl',
            name='short_url',
            field=models.CharField(max_length=32, default='aaa', verbose_name='Tiny url'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='tinyurl',
            name='real_url',
            field=models.URLField(verbose_name='Real url'),
        ),
    ]
