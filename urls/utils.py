from urls.models import TinyUrl
from django.conf import settings

import hashlib


def get_shorten_url(url):
    same_urls = TinyUrl.objects.filter(real_url=url)

    if same_urls.count() > 0:  # check if someone used the same url earlier
        return same_urls[0].short_url
    else:  # create new shortcut
        new_tiny_url = TinyUrl()
        new_tiny_url.real_url = url
        new_tiny_url.short_url = create_new_shorten_url(url)
        new_tiny_url.save()

        return new_tiny_url.short_url


def create_new_shorten_url(url):
    hex_value = hashlib.sha1(url.encode('utf-8')).hexdigest()  # hex value of url

    i = 4  # basic length of shortcut
    while True:
        new_url = settings.SITE_URL + hex_value[:i]
        if TinyUrl.objects.filter(short_url=new_url).exists():  # if identical shortcut exists get more chars from hex value
            i += 1
        else:
            return new_url

