from django import forms


class CutUrlForm(forms.Form):
    url_field = forms.URLField()
